import datetime as dt
import hashlib
import uuid

from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils import timezone


def default_expiry():
    return timezone.now() + dt.timedelta(days=1)


class Review(models.Model):
    rating = models.IntegerField(validators=[MaxValueValidator(5), MinValueValidator(1)])
    title = models.CharField(max_length=64)
    summary = models.TextField(max_length=10000)
    submitter_ip_addr = models.GenericIPAddressField()
    submission_date = models.DateTimeField(auto_now_add=True)
    company = models.CharField(max_length=100)
    reviewer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return "%d: %s by %s for %s" % (self.id, "⭐"*self.rating, self.reviewer.username, self.company)


class Token(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    hash_value = models.CharField(max_length=64)
    expires = models.DateTimeField(default=default_expiry) 

    @classmethod
    def get_bearer_token(cls, user):
        if cls.objects.filter(user=user).exists():
            token = cls.objects.get(user=user)
            if token.valid():
                return token
            else:
                token.delete()

        token = cls(user=user)
        token.hash_value = hashlib.sha256(token.id.bytes).hexdigest()
        token.save()
        return token

    def valid(self):
        return (self.expires > timezone.now())
