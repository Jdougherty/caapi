from django.contrib import admin
from .models import Review, Token

admin.site.register(Review)
admin.site.register(Token)
