from django.contrib.auth import authenticate
from django.forms.models import model_to_dict
from django.http.response import HttpResponseBadRequest, JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, View

from .forms import ReviewForm
from .http import HttpResponseUnauthorized
from .models import Review, Token

class RootView(TemplateView):
    template_name = 'reviews/root.html'


class CsrfExemptMixin():
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class TokenRequiredMixin():
    def dispatch(self, request, *args, **kwargs):
        if 'Authorization' not in request.headers:
            return HttpResponseUnauthorized()
        try:
            token = Token.objects.get(hash_value=request.headers.get('Authorization'))
            if not token.valid():
                token.delete()
                return HttpResponseUnauthorized()
        except Token.DoesNotExist:
            return HttpResponseUnauthorized()

        self.token_user = token.user
        return super().dispatch(request, *args, **kwargs)


class TokenApi(CsrfExemptMixin, View):
    http_method_names = ['post']
    
    def post(self, request, *args, **kwargs):
        username, password = request.POST.get('username'), request.POST.get('password')
        if not username or not password:
            return HttpResponseBadRequest()

        user = authenticate(username=username, password=password)
        if user is not None: 
            token = Token.get_bearer_token(user)
            return JsonResponse({'token': token.hash_value})
        else:
            return HttpResponseUnauthorized()

class ReviewApi(CsrfExemptMixin, TokenRequiredMixin, View):
    http_method_names = ['get', 'post']
    read_fields = ['rating', 'title', 'summary', 'submitter_ip_addr', 'submission_date', 'company', 'reviewer__username', 'submission_date']

    def get(self, request, *args, **kwargs):
        reviews = list(Review.objects.filter(reviewer=self.token_user).values(*self.read_fields))
        return JsonResponse(reviews, safe=False)

    def post(self, request, *args, **kwargs):
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(commit=False)
            review.reviewer = self.token_user
            review.submitter_ip_addr  = request.META.get('HTTP_X_FORWARDED_FOR') or request.META.get('REMOTE_ADDR')
            review.save()
            return JsonResponse(list(Review.objects.filter(id=review.id).values(*self.read_fields)), safe=False)
        else:
            return HttpResponseBadRequest(str(form.errors))


