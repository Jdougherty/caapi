import datetime as dt

from django.contrib.auth.models import User
from django.test import RequestFactory, TestCase
from django.utils import timezone
from django.urls.base import resolve

from mock_data import mock_data

from .models import Review, Token
from .views import ReviewApi, TokenApi


class TokenTestCase(TestCase):
    def setUp(self):
        mock_data()
        self.factory = RequestFactory()
        self.user = User.objects.first()


    def test_token__request_incorrect_params(self):
        request = self.factory.post('/tokens/')
        response = TokenApi.as_view()(request)
        self.assertEqual(response.status_code, 400)

    def test_token_request_incorrect_password(self):
        request = self.factory.post('/tokens/', {'username': 'Stacey', 'password': 'wrongpassword'})
        response = TokenApi.as_view()(request)
        self.assertEqual(response.status_code, 401)

    def test_token_request(self):
        request = self.factory.post('/tokens/', {'username': 'Stacey', 'password': 'es1Bahgoo'})
        response = TokenApi.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertIn('token', response.content.decode('utf-8'))

    def test_repeat_token_request(self):
        request = self.factory.post('/tokens/', {'username': 'Stacey', 'password': 'es1Bahgoo'})
        response = TokenApi.as_view()(request)
        self.assertEqual(response.status_code, 200)
        token = response.content

        response = TokenApi.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, token)

    def test_token_request_after_expiration(self):
        token = Token.get_bearer_token(self.user)
        token.expires = timezone.now() - dt.timedelta(days=1)
        token.save()
        request = self.factory.post('/tokens/', {'username': 'Stacey', 'password': 'es1Bahgoo'})
        response = TokenApi.as_view()(request)
        self.assertEqual(response.status_code, 200)




class ReviewTestCase(TestCase):
    def setUp(self):
        mock_data()
        self.factory = RequestFactory()
        self.user = User.objects.first()
        self.token = Token.get_bearer_token(self.user)

    def test_read_reviews(self):
        request = self.factory.get('/reviews/', HTTP_AUTHORIZATION=self.token.hash_value)
        response = ReviewApi.as_view()(request)
        self.assertEqual(response.status_code, 200)
        
    def test_review_api_invalid_token(self):
        request = self.factory.get('/reviews/', HTTP_AUTHORIZATION='12345')
        response = ReviewApi.as_view()(request)
        self.assertEqual(response.status_code, 401)

    def test_review_api_missing_token(self):
        request = self.factory.get('/reviews/')
        response = ReviewApi.as_view()(request)
        self.assertEqual(response.status_code, 401)

    def test_write_review(self):
        request = self.factory.post(
            '/reviews/', 
            {
                'rating': 1,
                'title': 'Test Title',
                'summary': 'Foo is a baz qux',
                'company': 'Test, Inc.',
            }, 
            HTTP_AUTHORIZATION=self.token.hash_value)
        response = ReviewApi.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_write_review_bad_params(self):
        request = self.factory.post(
            '/reviews/', 
            {
                'rating': 'eleven',
                'title': 'Test Title',
                'summary': 'Foo is a baz qux',
                'company': 'Test, Inc.',
                'extra_field': 'Foo',
            }, 
            HTTP_AUTHORIZATION=self.token.hash_value)
        response = ReviewApi.as_view()(request)
        self.assertEqual(response.status_code, 400)

    def test_write_review_bad_rating(self):
        request = self.factory.post(
            '/reviews/', 
            {
                'rating': 6,
                'title': 'Test Title',
                'summary': 'Foo is a baz qux',
                'company': 'Test, Inc.',
            }, 
            HTTP_AUTHORIZATION=self.token.hash_value)
        response = ReviewApi.as_view()(request)
        self.assertEqual(response.status_code, 400)

    def test_review_api_expired_token(self):
        self.token.expires = timezone.now() - dt.timedelta(days=1)
        self.token.save()
        request = self.factory.post(
            '/reviews/', 
            {
                'rating': 1,
                'title': 'Test Title',
                'summary': 'Foo is a baz qux',
                'company': 'Test, Inc.',
            }, 
            HTTP_AUTHORIZATION=self.token.hash_value)
        response = ReviewApi.as_view()(request)
        self.assertEqual(response.status_code, 401)


class ReviewAdminTestCase(TestCase):
    def setUp(self):
        mock_data()
        self.factory = RequestFactory()
        self.user = User.objects.create_superuser(username='admin',
                                 email='admin@email.com',
                                 password='verysecurepassword')

    def test_admin_review_interface(self):
        url = '/admin/reviews/review/'
        request = self.factory.get(url)
        request.user = self.user
        admin_view = resolve(url)
        response = admin_view.func(request)
        response.render()
        self.assertEqual(response.status_code, 200)
