from django.http.response import HttpResponse

class HttpResponseUnauthorized(HttpResponse):
    status_code = 401
