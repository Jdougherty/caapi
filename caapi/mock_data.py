from django.contrib.auth.models import User
from reviews.models import Review

def mock_data():
    user = User.objects.create_user('Stacey','StaceyRFernandez@email.com','es1Bahgoo')
    review = Review(
        rating=1,
        title='Lorem ipsum',
        summary='dolor sit amet...',
        submitter_ip_addr='127.0.0.1',
        company='Acme',
        reviewer=user
        )
    review.save()

    review = Review(
        rating=3,
        title='uis ultricies a arcu nec ultricies',
        summary='Aliquam ut ultrices velit. Proin dictum scelerisque porttitor.',
        submitter_ip_addr='127.0.0.1',
        company='Google',
        reviewer=user
        )
    review.save()

    review = Review(
        rating=5,
        title='Vivamus aliquet massa sit amet ',
        summary='Aliquam erat volutpat. ',
        submitter_ip_addr='127.0.0.1',
        company='Apple',
        reviewer=user
        )
    review.save()


    user = User.objects.create_user('Philip','PhilipMHart@teleworm.us eeZiex7u')
    review = Review(
        rating=2,
        title='Sed facilisis urna quis urna fringilla ornare.',
        summary='Phasellus efficitur ex quis interdum ullamcorper. Etiam orci dolor, pharetra sed sem ut, fringilla vehicula lectus. Morbi feugiat gravida dictum. Proin vitae aliquet tellus, sed gravida massa. Donec velit urna, pulvinar eu enim eget, porttitor lacinia dolor. In in suscipit ligula. Morbi molestie quis eros et bibendum. Sed efficitur, sapien ut sodales mollis, sem lacus efficitur ipsum, sed bibendum nulla magna feugiat massa.',
        submitter_ip_addr='127.0.0.1',
        company='Microsoft',
        reviewer=user
        )
    review.save()

    review = Review(
        rating=3,
        title='uis ultricies a arcu nec ultricies',
        summary='Aliquam ut ultrices velit. Proin dictum scelerisque porttitor.',
        submitter_ip_addr='127.0.0.1',
        company='Amazon',
        reviewer=user
        )
    review.save()

    review = Review(
        rating=4,
        title='Aliquam eleifend purus sed nibh vulputate ',
        summary='Interdum et malesuada fames ac ante ',
        submitter_ip_addr='127.0.0.1',
        company='Sony',
        reviewer=user
        )
    review.save()


    user = User.objects.create_user('Keith',' KeithWCortes@email.com','outhei1Ei')
    review = Review(
        rating=5,
        title='Sed luctus ipsum tellus, ut facilisis diam maximus id.',
        summary=' Nulla facilisi. Donec tincidunt ante nec lorem',
        submitter_ip_addr='127.0.0.1',
        company='Walmart',
        reviewer=user
        )
    review.save()

    review = Review(
        rating=5,
        title='Etiam dolor mi',
        summary=' cursus vel mauris sit amet, finibus cursus tellus.',
        submitter_ip_addr='127.0.0.1',
        company='Google',
        reviewer=user
        )
    review.save()

    review = Review(
        rating=1,
        title='Cras vel semper turpis. ',
        summary='Class aptent taciti sociosqu ad litora torquent per conubia nostra',
        submitter_ip_addr='127.0.0.1',
        company='Apple',
        reviewer=user
        )
    review.save()


    user = User.objects.create_user('Jenna',' JennaDRudy@email.com','ahPhoh3oshe')
    review = Review(
        rating=4,
        title='Vivamus tincidunt sem non varius viverra.',
        summary='Phasellus tempus nulla augue, ac maximus urna tempor eu.',
        submitter_ip_addr='127.0.0.1',
        company='Acme',
        reviewer=user
        )
    review.save()


    user = User.objects.create_user('Willie','WillieMDuffey@email.com','eeHo3Eih0ai')
    review = Review(
        rating=1,
        title='Nullam et massa mi. ',
        summary='Etiam viverra metus auctor laoreet sollicitudin',
        submitter_ip_addr='127.0.0.1',
        company='Staples',
        reviewer=user
        )
    review.save()


    user = User.objects.create_user('Lorraine','LorraineSGowin@email.com',' Aengein8ie')
    review = Review(
        rating=1,
        title='Phasellus aliquam enim at vestibulum blandit.',
        summary='Vivamus eu accumsan dolor. V',
        submitter_ip_addr='127.0.0.1',
        company='Petco',
        reviewer=user
        )
    review.save()

    review = Review(
        rating=3,
        title='ed ornare consequat dolor, ut pretium lacus',
        summary=' Etiam tristique lorem at elit sodales, nec fermentum arcu tincidunt. ',
        submitter_ip_addr='127.0.0.1',
        company='Google',
        reviewer=user
        )
    review.save()

if __name__ == '__main__':
    mock_data()