App Setup
=========

1) Create and activate a python3 venv: 
```
    python3 -m venv ./caapi-venv
    source ./caapi-venv/bin/activate
```
The `venv` module may need to be installed if you have not used it before. 
On Ubuntu, you can install this module with `apt-get install python3-venv`

2) Clone the repository
```
    git clone https://gitlab.com/Jdougherty/caapi.git
    cd caapi
```
3) Install the dependencies
```
    pip install -r requirements.txt 
```
4) Initialize the database
```
    caapi/manage.py migrate
```
Data Setup
==========

1) Populate the database 
```
    caapi/manage.py shell
    >>> import mock_data
    >>> mock_data.mock_data()
```

Usage
=====

1) Run the development server
```
    caapi/manage.py runserver
```
2) Navigate to 127.0.0.1:8000 in your browser to verify the server is running


API
===

## Obtaining a Token

To obtain an API token for a user, send a POST request to `/tokens/` 
with two parameters, `username` and `password`, for example:

    curl -X POST \
         -d "username=Stacey&password=es1Bahgoo" \
         http://127.0.0.1:8000/token/

The response is a JSON object with one key, `token`:

    {"token": "3dfc0c60c023a932b1c4d8dc686451cd9be289bdf693ba3bbc925fe815acbd6a"}

## Retrieving Reviews
To retrieve a user's reviews, send a GET request to `/reviews/`. 
The request must contain the HTTP `Authorization: ` header with a valid user token as the value,
for example:  

    curl -H 'Authorization: 3dfc0c60c023a932b1c4d8dc686451cd9be289bdf693ba3bbc925fe815acbd6a' \
         http://127.0.0.1:8000/reviews/

The response is a list of review instances represented as JSON objects, 

    [{"rating": 1, "title": "This is the review title", "summary": ... }]

## Writing Reviews
To submit a review, send a POST request to `/reviews/` with the following parameters:

   * rating: Integer within [1,5]
   * title: String max length 64
   * summary: String max length 10000
   * company: String max length 100

Again, a valid token must be passed in the HTTP Authorization header. For example:

    curl -X POST \
         -H 'Authorization: 3dfc0c60c023a932b1c4d8dc686451cd9be289bdf693ba3bbc925fe815acbd6a' \
         -d 'rating=1&title=Review Title&summary=This is a review summary&company=Acme'   \
         http://127.0.0.1:8000/reviews/

The response is a list with a single element, the newly created review record represented 
as a JSON object:

    [{"rating": 1, "title": "Review Title", "summary": "This is a review summary", 
    "submitter_ip_addr": "127.0.0.1", "submission_date": "2019-05-03T16:16:04.143Z", ... 
    }]


Test Suite
==========

To run the test suite:
```
    cd caapi/
    ./manage.py test
```
To verify code coverage
```
    coverage run --source '.' manage.py test
    coverage html
```
This will create an `htmlcov` directory containing an `index.html` file; open this file 
in your browser to view the coverage report. 


Admin View
==========

1) Create a superuser
```
    caapi/manage.py createsuperuser
```

2) With the development server running, navigate to http://127.0.0.1/admin

3) Login with the newly-created superuser credentials